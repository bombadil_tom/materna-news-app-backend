// tools.js
// ========
module.exports = {

  pushItem : function(item) {
    var AWS = require('aws-sdk');
    AWS.config.update({
      region: "eu-central-1"
    });
    var sns = new AWS.SNS();
		var payload = {
      default: item.title,
      APNS: {
        aps: {
          alert: item.title,
          sound: 'default',
          badge: 1,
          custom: {'action':'updated','channelID':'blogNews'}
        }
      }
    };
    // first have to stringify the inner APNS object...
    payload.APNS = JSON.stringify(payload.APNS);
    // then have to stringify the entire message payload
    payload = JSON.stringify(payload);
      var params = {
          MessageStructure: 'json',
          Message: payload,
          TopicArn: "arn:aws:sns:eu-central-1:041775737713:maternanewsapp_alldevices_MOBILEHUB_2102038192"
      };
      console.log(params);
      sns.publish(params, function(err, data) {
      if(err) {
          console.error('error publishing to SNS: '+err);
      } else {
          console.info('message published to SNS');
      }}
    );
  },

  storeItem : function (item)
  {
    if (item!=null)
    {
      var AWS = require('aws-sdk');
      AWS.config.update({
        region: "eu-central-1"
      });
      var db = new AWS.DynamoDB();
      var docClient = new AWS.DynamoDB.DocumentClient();	
      docClient.put({
      "TableName": "maternanewsapp-mobilehub-2102038192-News",
      "Item" : item}, function(err, data) {
          if (err) {
            console.log(err);
          }
          else {
              console.log('database entry created for '+item.guid);
          }
      });			
    }
  },

  ifItemNotExists : function (item, callback)
  {
    if (item!=null)
    {
      var AWS = require('aws-sdk');
      AWS.config.update({
        region: "eu-central-1"
      });
      var db = new AWS.DynamoDB();
      var docClient = new AWS.DynamoDB.DocumentClient();	
      docClient.get({
      "TableName": "maternanewsapp-mobilehub-2102038192-News",
      "Key" : {
          "userId": item.userId,
          "articleId": item.articleId
        }
      }, function(err, data) {
          if (err) {
            console.log(err);
          }
          else if (data.Item) {
            console.log('feed item already exists: '+data.Item.articleId);
          }
          else
          {
            console.log('creating feed item: '+item.articleId);
            callback(item);
          }
      });			
    }
  }
};
