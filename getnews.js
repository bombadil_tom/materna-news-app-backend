'use strict';

var AWS = require('aws-sdk');
AWS.config.update({
	region: "eu-central-1"
});

module.exports.getnews = function(event, context, cb) {
  var db = new AWS.DynamoDB();
  var docClient = new AWS.DynamoDB.DocumentClient();	
  var params = {
    TableName : "maternanewsapp-mobilehub-2102038192-News",
    KeyConditionExpression: "#uid=:materna",
    ExpressionAttributeNames: {
        "#uid": "userId",
    },
    ExpressionAttributeValues: {
        ":materna": "/materna/blog"
    },
  };

  docClient.query(params, function(err, data) {
    if (err) {
        console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
        context.fail(err);
    } else {
        console.log("Query succeeded.");
        context.done(null, data.Items);
    }
  });
};
