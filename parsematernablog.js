'use strict';

var tools = require('./tools.js');

module.exports.parsematernablog = function(event, context, cb) {

	console.log('Starting handler');

	var options = {};
	
	var finished = false;

	var FeedParser = require('feedparser')  
	  , request = require('request');

	var req = request('http://blog.materna.de/feed/')  
	  , feedparser = new FeedParser([options]);

	req.on('error', function (error) {  
		console.log("req.on.error.error="+error);
	  context.fail(error);
	});

	req.on('response', function (res) {  
  	  console.log("req.on.response.statuscode="+res.statusCode);
	  var stream = this;
	  if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));

	  stream.pipe(feedparser);
	});

	feedparser.on('error', function(error) {  
		console.log("feedparser.on.error.error="+error);
    context.fail(error);
	});

	feedparser.on('end', function() {  
		console.log("feedparser.on.end");
		finished = true;
	});

	feedparser.on('readable', function() {  
	  console.log("feedparser.on.readable");
	  // This is where the action is! 
	  var stream = this
	    , meta = this.meta
	    , feeditem;

	  while (feeditem = stream.read()) {
			var item = 	{ userId : "/materna/blog",
          articleId : feeditem.guid,
          title: feeditem.title,
          author: feeditem.author,
          //"category": feeditem.categories,
          content: feeditem.description,
          url: feeditem.url
      };
			tools.ifItemNotExists(item, function(item) { 
					tools.storeItem(item);
					tools.pushItem(item);
			});
		}
    });

};

