'use strict';

var tools = require('./tools.js');

module.exports.importxml = (event, context, callback) => {
  
  var input=event.Records[0].s3;
  var xml2js = require('xml2js');
  var parser = new xml2js.Parser();

  var AWS = require('aws-sdk');
  AWS.config.update({
    region: "eu-central-1"
  });
  var s3 = new AWS.S3();
  s3.getObject({ Bucket: input.bucket.name, Key: input.object.key }, function(err, data)
    {
      if (!err)
      {
          var xml=data.Body.toString();
          parser.parseString(xml, function (err, result) {
              var xmlitem=result.mobileContent.message[0];
              var item = 	{ userId : '/materna/blog',
                  articleId : xmlitem.head[0].id[0],
                  title: xmlitem.head[0].subject[0],
                  //author: feeditem.author,
                  category: xmlitem.head[0].channel[0],
                  content: xmlitem.body[0].slide[0].text[0]
                  // url: feeditem.url
              };
              console.log(item);
              tools.ifItemNotExists(item, function(item) { 
                  tools.storeItem(item);
                  tools.pushItem(item);
              });
              var params = {
                Bucket: input.bucket.name, 
                Delete: { // required
                  Objects: [ // required
                    {
                      Key: input.object.key // required
                    }
                  ],
                },
              };
              s3.deleteObjects(params, function(err, data) {
                if (err) console.log(err, err.stack); // an error occurred
                else     console.log(data);           // successful response
              });  
          });
      }
    });

  callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};
